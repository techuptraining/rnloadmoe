import React, { Component } from 'react'
import { Text, View, FlatList, Image, ActivityIndicator } from 'react-native'

export default class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: [],
      page: 1,
      isLoading: false
    }
  }

  renderRow = ({ item }) => {
    return (
      <View>
        <Image source={{ uri: item.url }} style={{ width: '100%', height: 100, resizeMode: 'cover' }} />
        <Text>{item.title}</Text>
        <Text>{item.id}</Text>
      </View>

    )
  }

  getData = async () => {
    const apiURL = 'https://jsonplaceholder.typicode.com/photos?_limit=15&_page=' + this.state.page;
    fetch(apiURL)
      .then((res) => res.json())
      .then((resJson) => {
        this.setState({
          data: this.state.data.concat(resJson),
          isLoading: false
        })
      })
  }

  componentDidMount() {
    this.setState({ isLoading: true }, this.getData)
  }

  handleLoadMore = () => {
    this.setState({ page: this.state.page + 1, isLoading: true }, this.getData)
  }

  renderFooter = () => {
    return (
      this.state.isLoading ?
        <ActivityIndicator size='large' />
        : null
    )
  }

  render() {
    console.log(this.state.page);
    return (
      <View>
        <FlatList
          data={this.state.data}
          renderItem={this.renderRow}
          keyExtractor={(item, index) => index.toString()}
          onEndReached={this.handleLoadMore}
          // onEndReachedThreshold={0}
          ListFooterComponent={this.renderFooter}
        />
      </View>
    )
  }
}
